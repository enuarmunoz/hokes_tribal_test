import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_tribal/presentation/cubit/jokers_cubit.dart';

class JokePage extends StatelessWidget {
  const JokePage({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final JokersCubit bloc = BlocProvider.of<JokersCubit>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chistes'),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        height: size.height,
        child: BlocBuilder<JokersCubit, JokersState>(
          builder: (_, state) {
            return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  if (state is JokersErrorState)
                    Text(
                      state.message,
                      style: const TextStyle(color: Colors.red),
                    ),
                  if (state is JokersLoadingState)
                    const CircularProgressIndicator(),
                  _createListLoke(state)
                ]);
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: ElevatedButton(
            child: const Text('Click para cargar chiste'),
            onPressed: () => bloc.getNewJoke()),
      ),
    );
  }

  Widget _createListLoke(JokersState state) {
    return Expanded(
      child: ListView.separated(
          itemBuilder: (_, index) =>
              ListTile(title: Text(state.jokes[index].value)),
          separatorBuilder: (_, index) => const Divider(),
          itemCount: state.jokes.length),
    );
  }
}
