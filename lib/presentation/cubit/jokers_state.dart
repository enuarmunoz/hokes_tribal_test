part of 'jokers_cubit.dart';

abstract class JokersState extends Equatable {
  const JokersState({this.jokes = const <JokeDom>[]});

  @override
  List<Object> get props => [];

  loading() => JokersLoadingState(jokes: jokes);

  final List<JokeDom> jokes;
}

class JokersInitState extends JokersState {}

class JokersLoadingState extends JokersState {
  const JokersLoadingState({required super.jokes});
}

class JokersErrorState extends JokersState {
  final String message;
  const JokersErrorState(this.message);
}

class JokersSuccessState extends JokersState {
  const JokersSuccessState(List<JokeDom> jokes) : super(jokes: jokes);
}
