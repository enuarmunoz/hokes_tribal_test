import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:test_tribal/core/failure/failure.dart';
import 'package:test_tribal/core/usecase/query.dart';
import 'package:test_tribal/domain/entities/joke_dom.dart';
import 'package:test_tribal/domain/usecases/get_jokes_usecase.dart';
import 'package:test_tribal/domain/usecases/get_new_joke_usecase.dart';

part 'jokers_state.dart';

class JokersCubit extends Cubit<JokersState> {
  JokersCubit(this._getNewJokeUserCase, this._getJokesUserCase)
      : super(JokersInitState());

  final GetNewJokeUserCase _getNewJokeUserCase;
  final GetJokesUserCase _getJokesUserCase;

  Future<void> getNewJoke() async {
    emit(state.loading());
    final Either<Failure, JokeDom> result =
        await _getNewJokeUserCase.execute(NoParams());
    result.fold((Failure failure) => emit(JokersErrorState(failure.message)),
        (_) async {
      final list = await _getJokesUserCase.execute(NoParams());
      emit(JokersSuccessState(list));
    });
  }
}
