import 'package:test_tribal/core/usecase/usecase.dart';

abstract class Query<Type, Params> extends UseCase<Type, Params> {}

class NoParams {}
