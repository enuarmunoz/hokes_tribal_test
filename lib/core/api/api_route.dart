mixin ApiRoute {
  static const String baseUrl = 'https://api.chucknorris.io';
  static const String jokers = '/jokes/random';
}
