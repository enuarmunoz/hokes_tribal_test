class Failure {
  final String message;
  Failure(this.message);
}

class ServerFailure extends Failure {
  ServerFailure(super.message);
}

class TimeOutFailure extends Failure {
  TimeOutFailure(super.message);
}

class UnknowFailure extends Failure {
  UnknowFailure(super.message);
}
