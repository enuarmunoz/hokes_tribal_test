// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:test_tribal/data/datasource/jokers_datasource.dart' as _i3;
import 'package:test_tribal/data/repositories/joke_remote_repository.dart'
    as _i5;
import 'package:test_tribal/domain/repositories/joke_repository.dart' as _i4;
import 'package:test_tribal/domain/usecases/get_jokes_usecase.dart' as _i6;
import 'package:test_tribal/domain/usecases/get_new_joke_usecase.dart'
    as _i7; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
extension GetItInjectableX on _i1.GetIt {
  // initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.JokerDataSource>(() => _i3.JokerDataSource());
    gh.factory<_i4.JokeRepository>(
        () => _i5.JokeRemoteRepository(gh<_i3.JokerDataSource>()));
    gh.factory<_i6.GetJokesUserCase>(
        () => _i6.GetJokesUserCase(gh<_i4.JokeRepository>()));
    gh.factory<_i7.GetNewJokeUserCase>(
        () => _i7.GetNewJokeUserCase(gh<_i4.JokeRepository>()));
    return this;
  }
}
