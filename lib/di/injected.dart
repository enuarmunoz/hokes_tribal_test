import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:test_tribal/di/injected.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: 'init', // default
)
void configureDependencies() => getIt.init();
