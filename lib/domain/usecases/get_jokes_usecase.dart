import 'package:injectable/injectable.dart';
import 'package:test_tribal/core/usecase/query.dart';
import 'package:test_tribal/domain/entities/joke_dom.dart';
import 'package:test_tribal/domain/repositories/joke_repository.dart';

@injectable
class GetJokesUserCase extends Query<List<JokeDom>, NoParams> {
  GetJokesUserCase(this._jokeRepository);
  final JokeRepository _jokeRepository;

  @override
  Future<List<JokeDom>> execute(NoParams params) =>
      _jokeRepository.getLocalJokes();
}
