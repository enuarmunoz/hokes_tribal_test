import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:test_tribal/core/extension/darttz_extension.dart';
import 'package:test_tribal/core/failure/failure.dart';
import 'package:test_tribal/core/usecase/query.dart';
import 'package:test_tribal/domain/entities/joke_dom.dart';
import 'package:test_tribal/domain/repositories/joke_repository.dart';

@injectable
class GetNewJokeUserCase extends Query<Either<Failure, JokeDom>, NoParams> {
  GetNewJokeUserCase(this._jokeRepository);
  final JokeRepository _jokeRepository;

  @override
  Future<Either<Failure, JokeDom>> execute(NoParams params) async {
    final Either<Failure, JokeDom> result = await _jokeRepository.getNewJoke();
    if (result.isRight()) {
      _jokeRepository.saveLocalJoke(result.asRight());
    }
    return result;
  }
}
