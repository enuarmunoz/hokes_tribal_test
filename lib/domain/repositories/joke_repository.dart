import 'package:dartz/dartz.dart';
import 'package:test_tribal/core/failure/failure.dart';
import 'package:test_tribal/domain/entities/joke_dom.dart';

abstract class JokeRepository {
  Future<Either<Failure, JokeDom>> getNewJoke();
  void saveLocalJoke(JokeDom joke);
  Future<List<JokeDom>> getLocalJokes();
}
