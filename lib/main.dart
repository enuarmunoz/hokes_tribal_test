import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_tribal/di/injected.dart';
import 'package:test_tribal/domain/usecases/get_jokes_usecase.dart';
import 'package:test_tribal/domain/usecases/get_new_joke_usecase.dart';
import 'package:test_tribal/presentation/cubit/jokers_cubit.dart';
import 'package:test_tribal/presentation/pages/joke_page.dart';

void main() {
  configureDependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) {
          return BlocProvider(
              child: const JokePage(),
              create: (context) {
                return JokersCubit(
                    getIt<GetNewJokeUserCase>(), getIt<GetJokesUserCase>());
              });
        }
      },
    );
  }
}
