import 'package:test_tribal/data/models/joke_dto.dart';
import 'package:test_tribal/domain/entities/joke_dom.dart';

extension JokeMapper on JokeDto {
  JokeDom toDom() => JokeDom(value);
}
