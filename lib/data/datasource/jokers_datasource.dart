import 'package:injectable/injectable.dart';
import 'package:test_tribal/domain/entities/joke_dom.dart';

@injectable
class JokerDataSource {
  factory JokerDataSource() {
    _instance ??= JokerDataSource._internal();
    return _instance!;
  }
  JokerDataSource._internal();
  static JokerDataSource? _instance;

  final List<JokeDom> _jokers = <JokeDom>[];

  void save(JokeDom jokeDom) => _jokers.add(jokeDom);
  List<JokeDom> findAll() => _jokers;
}
