class JokeDto {
  final String value;

  JokeDto(this.value);

  factory JokeDto.fromJson(Map<String, dynamic> json) {
    return JokeDto(json['value'] as String);
  }
}
