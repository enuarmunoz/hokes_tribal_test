import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:test_tribal/core/api/api_route.dart';
import 'package:test_tribal/data/datasource/jokers_datasource.dart';
import 'package:test_tribal/data/mappers/joke_mapper.dart';
import 'package:test_tribal/data/models/joke_dto.dart';
import 'package:test_tribal/domain/entities/joke_dom.dart';
import 'package:test_tribal/core/failure/failure.dart';
import 'package:test_tribal/domain/repositories/joke_repository.dart';
import 'package:http/http.dart' as http;

@Injectable(as: JokeRepository)
class JokeRemoteRepository implements JokeRepository {
  JokeRemoteRepository(this._jokeDataSource);
  final JokerDataSource _jokeDataSource;

  @override
  Future<Either<Failure, JokeDom>> getNewJoke() async {
    try {
      final http.Response response =
          await http.get(Uri.parse('${ApiRoute.baseUrl}${ApiRoute.jokers}'));
      final dto = JokeDto.fromJson(json.decode(response.body));
      return Right(dto.toDom());
    } on SocketException catch (_) {
      return Left(ServerFailure(_.toString()));
    } on TimeoutException catch (_) {
      return Left(TimeOutFailure(_.toString()));
    } catch (_) {
      return Left(UnknowFailure(_.toString()));
    }
  }

  @override
  Future<List<JokeDom>> getLocalJokes() =>
      Future.value(_jokeDataSource.findAll());
  @override
  void saveLocalJoke(JokeDom jokeDom) => _jokeDataSource.save(jokeDom);
}
